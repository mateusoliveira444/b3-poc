/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import bean.B3;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import static utils.DaoUtils.dataHoraAtual;

/**
 *
 * @author max A classe DaoFactory possui métodos que retornam Clientes de
 * Conexão com o WS Yank!
 */
public class B3Dao {

    /**
     * O método WSConnection é Estático e pode ser Chamado a qualquer momento,
     * de qualquer lugar. Ele retorna um cliente de conexão com o Web service
     * Yank!
     *
     * @return
     */
    private static OkHttpClient client;

    public B3Dao() {

        client = WSConnection();
    }

    public static OkHttpClient WSConnection() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(5, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES);
        OkHttpClient client = new OkHttpClient();
        client = builder.build();
        return client;
    }

    /**
     * O método WSProxyConnection é estático e pode ser chamado a qualquer
     * momento de qualquer lugar. Igual ao método WSConnection ele retorna um
     * cliente de conexão com o WS Yank! porem é utilizado quando a rede usa
     * proxy. O método recebe por parametro o Endereço de proxy e a porta.
     *
     * @param proxyAdress
     * @param port
     * @return
     */
    public void realizaInsert(B3 noticia) throws JSONException {
        String tabela = "yank_00002279";
        JSONObject jo = null;

        jo = new JSONObject();

        JSONArray ja = new JSONArray();

        jo.put("DATA_POSICAO_ACIONARIA", noticia.getDataPosicaoAcionaria());
        jo.put("VALOR_ACAO", noticia.getValorAcao());
        jo.put("DATA_DOCUMENTO", noticia.getDataDocumento());
        jo.put("PROTOCOLO", noticia.getProtocolo());
        jo.put("DATA_ENVIO", noticia.getDataEnvio());
        jo.put("HORA", noticia.getHora());
        jo.put("RAZAO_SOCIAL", noticia.getRazaoSocial());
        jo.put("DADOS_NOTICIA", noticia.getDadosNoticia());
        jo.put("DRI_SIGLA", noticia.getDri_sigla());
        jo.put("DOCUMENTO", noticia.getDocumento());
        jo.put("EVENTO", noticia.getEvento());
        jo.put("LINK", noticia.getLink());
        jo.put("TIME_EXEC", "NOW()");
        jo.put("TIME_IMPORT", dataHoraAtual());
        jo.put("STATUS_EXECUCAO", noticia.getStatusExec());
        jo.put("STATUS", 1);
        jo.put("INSTANCIA", 1);
        ja.put(jo);

        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, ja.toString());
        Request request = new Request.Builder()
                .url("http://workflow.yanksolutions.com.br/ws/api/v1/yanksolutions/post/inserts/" + tabela)
                .post(body)
                .addHeader("username", "yank")
                .addHeader("password", "Y@nk123")
                .addHeader("content-type", "application/json")
                .addHeader("cache-control", "no-cache")
                .build();

        Response response = null;
        String json = null;
        while (true) {
            try {
                response = client.newCall(request).execute();
                json = response.body().string();
                break;
            } catch (Exception e) {
                System.out.println("ERRO AO CONECTAR COM BD YANK: " + e);
                e.printStackTrace();
            }
        }

        response.close();

        try {
            JSONObject jObjRetornoWS = new JSONObject(json);
            jObjRetornoWS.get("mensagem").toString();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("ERRO AO CAPTURAR RETORNO DO WS AO INSERT");
        }

        System.out.println("STATUS: DADOS INSERIDOS COM SUCESSO!");
    }

}
