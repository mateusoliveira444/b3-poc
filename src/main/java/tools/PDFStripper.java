package tools;

import bean.B3;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Mateus Oliveira
 */
public class PDFStripper {

    public static B3 pdfStripper(B3 noticia, String nome) throws IOException, InterruptedException {
        Thread.sleep(2000);
        PDDocument document = PDDocument.load(new File("C:\\Users\\Mateus Oliveira\\Downloads\\"+nome+".pdf"));
        document.getClass();
        if (!document.isEncrypted()) {

            PDFTextStripperByArea stripper = new PDFTextStripperByArea();
            stripper.setSortByPosition(true);

            PDFTextStripper tStripper = new PDFTextStripper();

            String pdfFileInText = tStripper.getText(document);
            //System.out.println("Text:" + st);

            // split by whitespace
            String lines[] = pdfFileInText.split("\\n");
            String[] allStrings = new String[lines.length];

            for (int i = 0; i < allStrings.length; i++) {
                allStrings[i] = "";
            }

            int cont = 0;
            boolean pulalinha = false;
            for (int i = 0; i < lines.length; i++) {
                String line = lines[i];

                if (line.equals(" \r")) {
                    if (pulalinha) {
                        cont++;
                    }
                    pulalinha = false;
                    continue;
                }

                pulalinha = true;
                allStrings[cont] += line;

            }
            List<String> acoes = new ArrayList<>();
            for (int i = 0; i < allStrings.length; i++) {
                if (allStrings[i].matches("(?s)(.*[R$]\\s\\b[(\\d)].*|.*[R$]\\b[(\\d)].*)") && (allStrings[i].matches("(?s)(?i).*\\b\\sação\\b\\s.*") || allStrings[i].contains("(?s)(?i).*\\b\\sações\\b\\s.*"))) {
                    int index = allStrings[i].indexOf("por ação");
                    System.out.println("allStrings[i] = " + allStrings[i]);
                    allStrings[i] = allStrings[i].substring(index - 45, index + 45);
                    allStrings[i] = allStrings[i].replaceAll("[^(\\d) |^(,)]", "");
                    allStrings[i] = allStrings[i].replaceAll("(?<=[\\s])\\s*|^\\s+|\\s+$", "");
//                    allStrings[i] = allStrings[i].replace(" ,", "");
                    noticia.setValorAcao("R$ " + allStrings[i].replaceAll(" ,", "").replaceAll(" .", ""));
                    System.out.println("V A L O R  D A  A Ç Ã O:" + noticia.getValorAcao());
                    if (allStrings[i].length() < 4) {
                        continue;
                    }
                    acoes.add(allStrings[i]);
                    break;
                }

            }

        }
        if (noticia.getValorAcao() == null) {
            noticia.setValorAcao("VALOR DA AÇÃO NÃO ENCONTRADA NO DOCUMENTO");
        }
        return noticia;
    }

}
