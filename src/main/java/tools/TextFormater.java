/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;


import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.IOUtils;


/**
 *
 * @author YANK-MATHEUS
 */
public class TextFormater {

    static int varIntAux = 0;
    public static List<String> camposInvalidos = null;
    static int ano2Num;
    static int ano4Num;
    static Scanner scanner = new Scanner(System.in);

    public static void travaDeExecucao() {
        System.out.println("\n\n<><><><><><><><><><><><><><><><><><><><><>");
        System.out.println("ATENÇÃO: VALIDE AS AÇÕES DO ROBÔ E TECLE 'S' E <ENTER> NESTA TELA!");
        System.out.print("ENTRADA: ");
        scanner.next();
    }

    public static String retiraUltimaPalavra(String text) {
//        System.out.println("Tratando texto para retirar a ultima palavra.");
        String retorno = "";
//        System.out.println("O texto parseado contem " + text.length() + " caracteres");
        for (int leng = text.length(); leng > 0; leng--) {
//            System.out.println("posição: " + leng + " = " + text.substring(leng - 1, leng));
            if (text.substring(leng - 1, leng).equals(" ") || text.substring(leng - 1, leng).equals("\n") || text.substring(leng - 1, leng).equals("\t")) {
                retorno = slimText(text.substring(0, leng - 1));
                
                return retorno;
            } else {
                continue;
            }

        }
//        System.out.println("Texto é palavra única");
        return text;

    }

    public static String slimText(String text){
        int leng = text.length();
        if(leng> 0){
            while(text.substring(0, 1).equals(" ")||text.substring(0, 1).equals("\n")||text.substring(0, 1).equals("\t")){
                text= text.substring(1, text.length());
                
            }
            while(text.subSequence(text.length()-1, text.length()).equals(" ")||text.subSequence(text.length()-1, text.length()).equals("\n")||text.subSequence(text.length()-1, text.length()).equals("\t")){
                text = text.substring(0, text.length()-1);
            }
        return text;
        }else{
//            System.out.println("String vazia... retornand");
            return text;
        }
        
    }
    public static String getLastWord(String text) {
        String retorno = "";
        for (int leng = text.length(); leng > 0; leng--) {
            if (text.substring(leng - 1, leng).equals(" ") || text.substring(leng - 1, leng).equals("\n") || text.substring(leng - 1, leng).equals("\t")) {
                retorno = slimText(text.substring(leng, text.length()));
                return retorno;
            } else {
                continue;
            }

        }
        System.out.println("Texto é palavra única");
        text = text.replace(" ", "").replace("\n","").replace("\t","");

        return text;

    }


    public static Map<String, String> parserFormularioSubs(String rawForm) {
        Map<String, String> retorno = new HashMap<String, String>();
        rawForm.split("\n");
        return retorno;
    }


    /**
     *
     * @param source Caminho do arquivo txt
     * @return Conteúdo do arquivo txt em UTF8
     */
    public static String leituraTxtFile(String source) throws FileNotFoundException, IOException {
        FileInputStream file = null;

        try {
            file = new FileInputStream(source);
        } catch (Exception ex) {
            System.out.println("ERRO: NÃO ENCONTROU ARQUIVO: " + source);
            try {
                System.in.read();
            } catch (Exception e) {

            }
        }

        BufferedReader in = new BufferedReader(new InputStreamReader(file, StandardCharsets.ISO_8859_1));
        LinkedList<String> lines = new LinkedList();
        String readLine;
        try {
            while ((readLine = in.readLine()) != null) {
                lines.add(readLine);
            }
        } catch (IOException ex) {
            Logger.getLogger(TextFormater.class.getName()).log(Level.SEVERE, null, ex);
        }

        String strContentFile = "";
        for (String line : lines) {
            strContentFile = strContentFile + line + "\n";
        }
        return strContentFile;

    }

    /**
     * Realiza a leitura da string que contém a resposta escolhida para os
     * campos que são de múltipla escolha
     *
     * @param opcoesResposta São as opções do formulário separadas por \n
     * @return Retorna o texto da opção escolhida pelo usuário
     */
    private static String capturaOpcaoEscolhida(String opcoesResposta) {
        opcoesResposta = opcoesResposta.replace(" ", "");
        String[] opcoes = opcoesResposta.split("\\(X\\)");
        if (opcoes.length == 1) {
            return "";
        }
        //RETORNA O TEXTO QUE ESTÁ NA FRENTE DO (X)
        return opcoes[1].split("\n")[0].toUpperCase();
    }


    public static String formataData(String dataNasc) {
        
        dataNasc = dataNasc.replaceAll("[^\\d]", "");

        if (dataNasc.length() == 8) {
            dataNasc = dataNasc.substring(0, 2) + "/" + dataNasc.substring(2, 4) + "/" + dataNasc.substring(4, 8);
        }
        return dataNasc;
    }

    public static String capturaString() {
        Scanner scanner = new Scanner(System.in);
        String result = scanner.next();
        return result;
    }


    //FUNÇÃO PARA RECEBER UMA LINHA E CAPTURAR TODAS AS COLUNAS INDICADAS DESSA LINHA
    private static String capturaLinhaToda(HashMap<Integer, List<String>> dict, int linha, int[] colunas) {
        String conteudoLinha = "";
        //DICT É COLUNA E DEPOIS LINHA

        for (int c : colunas) {
            try {
                conteudoLinha += dict.get(c).get(linha);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return conteudoLinha;
    }



    public static boolean singleWordsTextMatch(String model, String match) {
        //retorna true se todas as palavras de model, estiverem contidas em match
        String modelContent = model.toUpperCase().replace(",", "").replace(".", "").replace("!", "");
        List<String> modelWords = new ArrayList<String>();
        boolean deuMatch = true;
        int countMatch = 0;
        while (modelContent.contains(" ") || modelContent.contains("\t") || modelContent.contains("\n")) {
            String matchWord = getLastWord(modelContent);
            modelWords.add(matchWord);
            System.out.println(matchWord + " adicionado;");
            modelContent = retiraUltimaPalavra(modelContent);
        }
        modelWords.add(getLastWord(modelContent));
        System.out.println(modelContent.replace("\n", "").replace("\t", "").replace(" ","") + " adicionado.");
        System.out.println("Procurando as " + modelWords.size() + " encontradas no modelo.");
        for (String response : modelWords) {
            if (match.toUpperCase().replace(",", "").replace(".", "").replace("!", "").contains(response)) {
                countMatch++;
                continue;
            } else {
                System.out.println("não encontramos a palavra: "+response);
                deuMatch = false;
            }
        }
        System.out.println("Foram encontradas " + countMatch + " de " + modelWords.size());
        System.out.println(deuMatch);
        return deuMatch;
    }
}
