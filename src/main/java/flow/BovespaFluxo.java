
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flow;

import bean.B3;
import dao.B3Dao;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import static tools.PDFStripper.pdfStripper;
import utils.SeleniumUtils;

import utils.SikuliUtils;

/**
 *
 * @author Mateus Oliveira
 */
public class BovespaFluxo {

    public static void bovespaFluxo() throws InterruptedException, AWTException, IOException {
        System.out.println("Iniciando Fluxo");
        SeleniumUtils chrome = new SeleniumUtils();
        chrome.buildChromeDriver();
        B3Dao noticiaDao = new B3Dao();
        B3 noticia;
        Robot robot = new Robot();
        List<String> filtroNoticias = new ArrayList<>();
        Integer arquivo = new Integer(0);
        //Tipo de noticia procurada no filtro de busca
        String[] filtroNoticia = {"Aviso aos acionistas"};
//        String[] filtroNoticia = {"Aviso aos acionistas", "Comunicado ao mercado", "Fato relevante", "Edital de convocação de AGE", "Ata reuniao do conselho de administracao"};

        //COMEÇA FLUXO
        //PREENCHE LISTA
        for (int i = 0; i < filtroNoticia.length; i++) {
            filtroNoticias.add(filtroNoticia[i]);
        }
        //CORRIGIR O ROBÔ PARA CAPTURAR DADOS DE TODO 
        // PERCORRE FLUXO PARA CADA TIPO DE BUSCA
        System.out.println("- - - - I N I C I A  F L U X O - - - -");
        for (String busca : filtroNoticias) {
            List<String> linkDocumentos = new ArrayList<>();

            chrome.getDriver().get("http://www2.bmfbovespa.com.br/Agencia-Noticias/ListarNoticias.aspx?idioma=pt-br&q=&tipoFiltro=0");
            Thread.sleep(1000);
            chrome.waitFor(By.xpath("//*[text() = 'Na última semana']"), 5).click();
            chrome.waitFor(By.id("txtTermoBusca"), 5).sendKeys(busca + "\n");
            //verifica se encontra noticia
            if (!chrome.waitForElements(By.className("box-aviso"), 5).isEmpty()) {
                continue;
            }
            while (true) {
                try {
                    WebElement divLinks = chrome.waitFor(By.id("linksNoticias"), 5);
                    List<WebElement> documentos = divLinks.findElements(By.tagName("a"));
                    for (WebElement documento : documentos) {
                        if (documento.getText().contains("(N)")) {
                            linkDocumentos.add(documento.getAttribute("href"));
                        }
                    }
                    chrome.waitFor(By.id("pagNav_listaNoticias_nextPage"), 5).click();
                } catch (Exception e) {
                    break;
                            
                }
            }
            String[] split = null;
            //salva documentos
            for (String linkDocumento : linkDocumentos) {
                noticia = new B3();
                chrome.getDriver().get(linkDocumento);
                chrome.waitFor(By.id("contentNoticia"), 5);
                WebElement noticias = chrome.waitFor(By.tagName("pre"), 5);
                split = noticias.getText().split("\n");
                System.out.println("split = " + split);

                if (split.length == 8) {
                    noticia.setEvento("Evento não encontrado");
                    noticia.setDadosNoticia(split[0]);
                    noticia.setStatusExec("EVENTO NÃO ENCONTRADO");
                    noticiaDao.realizaInsert(noticia);
                } else if (split.length >= 10) {
                    if (split[2].contains("A partir de")) {

                        noticia.setDataPosicaoAcionaria(split[2].split(",")[0]);
                    }
                    //(?i) IGNORA CASE SENSITIVE (.*) seleciona todas palavras anterior ou posterior
                    System.out.println("- - - - C A P T U R A  E V E N T O - - - -");
                    if (split[2].matches("(?i).*Dividendo.*") || split[2].matches("(?i).*Dividendos.*")) {
                        noticia.setEvento("DIVIDENDO");
                        noticia.setStatusExec("EVENTO ENCONTRADO");
                        noticia.setDadosNoticia(split[0]);

                    } else if (split[2].matches("(?i).*fracoes.*")) {
                        noticia.setEvento("Leilao de Fracoes");
                        noticia.setStatusExec("EVENTO ENCONTRADO");
                        noticia.setDadosNoticia(split[0]);

                    } else if (split[2].matches("(?i).*\\bSobras.*\\bde.*\\bSubscricao.*") || (split[2].matches("(?i).*\\bSobras.*\\bde.*\\bSubscricao.*"))) {
                        noticia.setEvento("Sobras de Subscricao");
                        noticia.setStatusExec("EVENTO ENCONTRADO");
                        noticia.setDadosNoticia(split[0]);

                    } else if (split[2].matches("(?i).*Sobras.*")) {
                        noticia.setEvento("Sobras de Subscricao");
                        noticia.setStatusExec("EVENTO ENCONTRADO");
                        noticia.setDadosNoticia(split[0]);

                    } else if (split[2].matches("(?i).*juros.*") || (split[2].matches("(?i).*juro.*"))) {
                        noticia.setEvento("Juros sobre capital proprio");
                        noticia.setStatusExec("EVENTO ENCONTRADO");
                        noticia.setDadosNoticia(split[0]);

                    } else {
                        noticia.setEvento("Evento não relevante");
                        noticia.setStatusExec(split[2]);
                        noticiaDao.realizaInsert(noticia);
                        continue;

                    }
                    System.out.println("- - - - A C E S S A  D O C U M E N T O - - - -");
                    String link = chrome.waitFor(By.xpath("//a[text() = 'Clique aqui e veja o documento na íntegra']"), 5).getAttribute("href").split("=")[1];
                    chrome.waitFor(By.xpath("//a[text() = 'Clique aqui e veja o documento na íntegra']"), 5).click();
                    chrome.getDriver().switchTo().frame("cabecalho");
                    WebElement textDocumento = chrome.waitFor(By.tagName("td"), 5);
                    System.out.println("textDocumento = " + textDocumento.getText());
                    String splitDadosDocumento[] = textDocumento.getText().split("\n");
                    noticia.setRazaoSocial(splitDadosDocumento[0]);
                    noticia.setDri_sigla(splitDadosDocumento[1]);
                    noticia.setDocumento(splitDadosDocumento[2]);
                    noticia.setDataDocumento(splitDadosDocumento[3].replaceAll("[a-zA-Z\\sb]+", ""));
                    noticia.setDataEnvio(splitDadosDocumento[4].split("-")[0]);
                    noticia.setProtocolo(splitDadosDocumento[4].split("-")[1]);

                    chrome.getDriver().get("http://www2.bmfbovespa.com.br/empresas/consbov/ArquivosExibe.asp?site=&protocolo=" + link);
                    noticia.setLink("http://www2.bmfbovespa.com.br/empresas/consbov/ArquivosExibe.asp?site=&protocolo=" + link);
                    chrome.switchFrame(-1);
                    chrome.waitFor(By.id("plugin"), 5).click();
                    //PREPARA CONTROL V
                    String text = arquivo.toString();
                    StringSelection stringSelection = new StringSelection(text);
                    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                    clipboard.setContents(stringSelection, stringSelection);
                    System.out.println("- - - - B A I X A  A R Q U I V O - - - -");
                    // SALVA ARQUIVO
                    robot.keyPress(KeyEvent.VK_CONTROL);
                    robot.keyPress(KeyEvent.VK_S);
                    robot.keyRelease(KeyEvent.VK_CONTROL);
                    robot.keyRelease(KeyEvent.VK_S);
                    Thread.sleep(2000);
                    robot.keyPress(KeyEvent.VK_CONTROL);
                    robot.keyPress(KeyEvent.VK_V);
                    robot.keyRelease(KeyEvent.VK_V);
                    robot.keyRelease(KeyEvent.VK_CONTROL);
                    robot.keyPress(KeyEvent.VK_ENTER);
                    robot.keyRelease(KeyEvent.VK_ENTER);

                    try {
                        System.out.println("- - - - E X T R A I  D A D O S  D O  A R Q U I V O - - - -");
                        pdfStripper(noticia, "text");
                    } catch (Exception e) {
                        noticia.setStatusExec("DOCUMENTO NÃO ENCONTRADO");
                    }
                    arquivo++;

                    noticiaDao.realizaInsert(noticia);

                }
            }
        }
    }

}
