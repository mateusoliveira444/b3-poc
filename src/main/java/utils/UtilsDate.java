/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Mateus Oliveira
 */
public class UtilsDate {

    public static String getYear() {
        DateFormat dateFormatYear = new SimpleDateFormat("yyyy");
        Date date = new Date();
        String dateYear = dateFormatYear.format(date);
        return dateYear;
    }

    public static String getMonth() {
        DateFormat dateFormatMonth = new SimpleDateFormat("MM");
        Date date = new Date();
        String dateMonth = dateFormatMonth.format(date);
        return dateMonth;
    }

    public static String getDay() {
        DateFormat dateFormatDay = new SimpleDateFormat("dd");
        Date date = new Date();
        String dateDay = dateFormatDay.format(date);
        return dateDay;
    }

    public static String getYesterday() {
        DateFormat dateFormatDay = new SimpleDateFormat("dd");
        String dateDay = dateFormatDay.format(yesterday());
        return dateDay;
    }

    
    public static Date yesterday() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return cal.getTime();
    }

    
    
    public String getLasttUilDay() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        int dayOfWeek;
        do {
            cal.add(Calendar.DAY_OF_MONTH, -1);
            dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
        } while (dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY);

        return dateFormat.format(cal.getTime());

    }

    public String getCurrentDay() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        return dateFormat.format(cal.getTime());

    }

}
