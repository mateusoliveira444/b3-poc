/*
    Classe Destinada à Utilidades e funções do Sistema Operacional.
    Manipulando arquivos, trabalhando com configurações de Sistema, identificando informações sobre o 
    ambiente de Execução do Robo.
 */
package utils;

import com.sun.jna.platform.win32.Advapi32Util;
import static com.sun.jna.platform.win32.WinReg.HKEY_LOCAL_MACHINE;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javafx.scene.control.TextFormatter;
import static org.apache.commons.io.IOUtils.buffer;
import static org.apache.commons.lang.StringUtils.split;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import tools.TextFormater;
import tools.UrlDownload;

/**
 *
 * @author max
 */
public class SystemUtils {

    public static Scanner scanner = new Scanner(System.in);

    public SystemUtils() {
    }

    SystemUtils system = new SystemUtils();

    public static String getOperationalSystem() {
        return System.getProperty("os.name");
    }

    public static String getSystemArch() {
        return System.getProperty("os.arch");
    }

    public static void setRegistryLifeSaver() {
        /*Para que o Internet Explorer funcione com performance no Selenium, é necessário Criar este 
        registro no REGEDIT do Windows para autorizar o IE a "escrever" em memoria cache.*/
        String path = "";
        if (SystemUtils.getSystemArch().contains("64")) {
            path = "Software\\Wow6432Node\\Microsoft\\Internet Explorer\\Main\\FeatureControl\\FEATURE_BFCACHE";
        } else {
            path = "Software\\Microsoft\\Internet Explorer\\Main\\FeatureControl\\FEATURE_BFCACHE";
        }

        if (Advapi32Util.registryKeyExists(HKEY_LOCAL_MACHINE, path)) {
            System.out.println("Registro Já inserido");
        } else {
            System.out.println("Registro não inserido. Criando");
            Advapi32Util.registryCreateKey(HKEY_LOCAL_MACHINE, path);
        }
        if (Advapi32Util.registryValueExists(HKEY_LOCAL_MACHINE, path, "iexplore.exe")) {
            System.out.println("Valores Já prontos");
        } else {
            System.out.println("Defindo Chave e valor");
            Advapi32Util.registrySetLongValue(HKEY_LOCAL_MACHINE, path, "iexplore.exe", 0);

        }
    }

    public static boolean checkCommandAnswer(String answer, String find) {
        if (answer.toUpperCase().contains(find.toUpperCase()) && !answer.toUpperCase().contains("COMMAND NOT FOUND")) {
            return true;
        } else {
            return false;
        }
    }

    public static String executeCommand(String command) {
        /*Executa comando no cmd do Windows ou terminal do Linux.
        Recebe ums String que deve conter o código a ser executado.*/

        if (getOperationalSystem().toUpperCase().contains("Windows")) {
            /*para o diferentão do Windows, o RunTime Java precisa Expecificar qual o Terminal desejado.
            concatenando o nome antes do comando juntamente com o prametro /c para que seja executado de 
            forma interna. (sem abrir a interface CMD)*/
            command = "cmd /c " + command;
        }

        StringBuffer output = new StringBuffer();

        Process p;
        try {
            p = Runtime.getRuntime().exec(command);
            p.waitFor();
            BufferedReader reader
                    = new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = "";
            while ((line = reader.readLine()) != null) {
                output.append(line + "\n");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return output.toString();

    }

    public static void deleteFiles(List<String> paths) {
        /*Deleta vários arquivos. Recebe lista de Strings que deve ser o caminho absoluto dos arquivos.*/
        for (String path : paths) {
            File dir = new File(path);
            try {
                dir.delete();
            } catch (Exception ex) {
                System.out.println(ex.toString());
            }
        }
    }

    public static void deleteFile(String path) {
        /*Deleta vários arquivos. Recebe uma String que deve conter o caminho absoluto do arquivo.*/
        File dir = new File(path);
        try {
            dir.delete();
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }
}
