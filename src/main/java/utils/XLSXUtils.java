/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFSheetConditionalFormatting;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import static org.apache.poi.ss.usermodel.CellType.BLANK;
import static org.apache.poi.ss.usermodel.CellType.ERROR;
import static org.apache.poi.ss.usermodel.CellType.FORMULA;
import static org.apache.poi.ss.usermodel.CellType.NUMERIC;
import static org.apache.poi.ss.usermodel.CellType.STRING;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author tmont
 */
public class XLSXUtils {

    private File file;
    private Workbook workbook;
    private Sheet sheet;
    private Row row;
    private Cell cell;

    public void createNewWorkbook(String path) throws FileNotFoundException, IOException, InvalidFormatException {
        setFile(new File(path));
        try {
            if (file.createNewFile()) {
                System.out.println("File is created!");
            } else {
                System.out.println("File already exists.");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        
        Workbook newWorkbook = new XSSFWorkbook();
        CreationHelper createHelper = newWorkbook.getCreationHelper();
        setWorkbook(newWorkbook);
        FileOutputStream fileOut = new FileOutputStream(file);
        workbook.write(fileOut);
        fileOut.close();

        // Closing the workbook
//        workbook.close();

    }
    public void saveWorkBook() throws FileNotFoundException, IOException{
        FileOutputStream fileOut = new FileOutputStream(file);
        workbook.write(fileOut);
        fileOut.close();
    }

    public void createNewSheet(String name) {
        Sheet newSheet = workbook.createSheet(name);
        setSheet(newSheet);
        sheet.createRow(0);
        setRow(0);
        row.createCell(0);
        setCell(0);
    }

    /**
     * @return the workbook
     */
    public Workbook getWorkbook() {
        return workbook;
    }

    /**
     * @param workbook the workbook to set
     */
    public void setWorkbook(Workbook workbook) {
        this.workbook = workbook;
    }

    /**
     * @return the sheet
     */
    public Sheet getSheet() {
        return sheet;
    }

    /**
     * @param sheet the sheet to set
     */
    public void setSheet(Sheet sheet) {
        this.sheet = sheet;
    }

    /**
     * @return the row
     */
    public Row getRow() {
        return row;
    }

    /**
     * @param row the row to set
     */
    public void setRow(Row row) {
        this.row = row;
    }

    /**
     * @param row the row to set
     */
    public void setRow(int row) {
        if(sheet.getRow(row)== null){
            sheet.createRow(row);
        }
        this.row = sheet.getRow(row);
    }

    /**
     * @return the cell
     */
    public Cell getCell() {
        return cell;
    }

    /**
     * @param cell the cell to set
     */
    public void setCell(Cell cell) {
        this.cell = cell;
    }

    /**
     * @param cell the cell to set
     */
    public void setCell(int cell) {
        if(row.getCell(cell)==null){
            row.createCell(cell);
        }
        this.cell = row.getCell(cell);
        
    }

    /**
     * @return the file
     */
    public File getFile() {
        return file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(File file) {
        this.file = file;
    }

}
