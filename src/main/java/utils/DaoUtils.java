/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import bean.B3;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
//import views.TabelaPrincipal;

/**
 *
 * @author Mateus Oliveira
 */
public class DaoUtils {
    // MÉTODO UTILIZADO CASO VOCÊ TENHA QUE MEXER COM TABELAS REFERNCIADAS, ESTE MÉTODO RETORNA TODOS OS IDS DA TABELA PRINCIPAL, 
    // E CONCATENA NUMA STRING E RETORNA ESTA PARA USAR NO OUTRO SELECT
    private static Calendar dataAtual = new GregorianCalendar();

    public static String concatenaSelect(List<String> select) {
        String concatenado = "";
        for (String s : select) {
            concatenado += (s + ",");
        }
        concatenado = concatenado.substring(0, concatenado.length() - 1);
        return concatenado;
    }

    public static String dataHoraAtual() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        dataAtual = new GregorianCalendar();
        Date agora = dataAtual.getTime();
        return sdf.format(agora);
    }

    //METODO PREPARA PARA UPDATE
//
    public static B3 preparaUpdate(int id, int status, String statusExec) {
        B3 tabela = new B3();
        
        return tabela;
    }
    public static B3 preparaInsertint () {
        B3 tabela = new B3();
        
        return tabela;
    }
}
