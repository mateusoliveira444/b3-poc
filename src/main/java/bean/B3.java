/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

/**
 *
 * @author Mateus Oliveira
 */
public class B3 {

    private String dataPosicaoAcionaria;
    private String dataDocumento;
    private String dataEnvio;
    private String hora;
    private String razaoSocial;
    private String dri_sigla;
    private String documento;
    private String evento;
    private String statusExec;
    private String dadosNoticia;
    private String protocolo;
    private String valorAcao;
    private String link;
    private int id;
    private int status;

  
    /**
     * @return the dataDocumento
     */
    public String getDataDocumento() {
        return dataDocumento;
    }

    /**
     * @param dataDocumento the dataDocumento to set
     */
    public void setDataDocumento(String dataDocumento) {
        this.dataDocumento = dataDocumento;
    }

    /**
     * @return the dataEnvio
     */
    public String getDataEnvio() {
        return dataEnvio;
    }

    /**
     * @param dataEnvio the dataEnvio to set
     */
    public void setDataEnvio(String dataEnvio) {
        this.dataEnvio = dataEnvio;
    }

    /**
     * @return the hora
     */
    public String getHora() {
        return hora;
    }

    /**
     * @param hora the hora to set
     */
    public void setHora(String hora) {
        this.hora = hora;
    }

    /**
     * @return the razaoSocial
     */
    public String getRazaoSocial() {
        return razaoSocial;
    }

    /**
     * @param razaoSocial the razaoSocial to set
     */
    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    /**
     * @return the dri_sigla
     */
    public String getDri_sigla() {
        return dri_sigla;
    }

    /**
     * @param dri_sigla the dri_sigla to set
     */
    public void setDri_sigla(String dri_sigla) {
        this.dri_sigla = dri_sigla;
    }

    /**
     * @return the documento
     */
    public String getDocumento() {
        return documento;
    }

    /**
     * @param documento the documento to set
     */
    public void setDocumento(String documento) {
        this.documento = documento;
    }

    /**
     * @return the evento
     */
    public String getEvento() {
        return evento;
    }

    /**
     * @param evento the evento to set
     */
    public void setEvento(String evento) {
        this.evento = evento;
    }

    /**
     * @return the statusExec
     */
    public String getStatusExec() {
        return statusExec;
    }

    /**
     * @param statusExec the statusExec to set
     */
    public void setStatusExec(String statusExec) {
        this.statusExec = statusExec;
    }

    /**
     * @return the dadosNoticia
     */
    public String getDadosNoticia() {
        return dadosNoticia;
    }

    /**
     * @param dadosNoticia the dadosNoticia to set
     */
    public void setDadosNoticia(String dadosNoticia) {
        this.dadosNoticia = dadosNoticia;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the protocolo
     */
    public String getProtocolo() {
        return protocolo;
    }

    /**
     * @param protocolo the protocolo to set
     */
    public void setProtocolo(String protocolo) {
        this.protocolo = protocolo;
    }

    /**
     * @return the dataPosicaoAcionaria
     */
    public String getDataPosicaoAcionaria() {
        return dataPosicaoAcionaria;
    }

    /**
     * @param dataPosicaoAcionaria the dataPosicaoAcionaria to set
     */
    public void setDataPosicaoAcionaria(String dataPosicaoAcionaria) {
        this.dataPosicaoAcionaria = dataPosicaoAcionaria;
    }

    /**
     * @return the valorAcao
     */
    public String getValorAcao() {
        return valorAcao;
    }

    /**
     * @param valorAcao the valorAcao to set
     */
    public void setValorAcao(String valorAcao) {
        this.valorAcao = valorAcao;
    }

    /**
     * @return the link
     */
    public String getLink() {
        return link;
    }

    /**
     * @param link the link to set
     */
    public void setLink(String link) {
        this.link = link;
    }
}
