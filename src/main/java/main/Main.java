package main;

import static flow.BovespaFluxo.bovespaFluxo;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.sikuli.script.FindFailed;
import utils.SystemUtils;

/**
 *
 * @author max
 */
public class Main {

    public static void main(String[] args) throws InterruptedException, IOException, FindFailed, Exception {
        
          bovespaFluxo();
    }
}
